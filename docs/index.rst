.. flow3r documentation master file, created by
   sphinx-quickstart on Sun Jun 11 19:43:11 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to flow3r's documentation!
==================================

.. toctree::
   :maxdepth: 1
   :caption: The badge:

   badge/hardware_specs.rst
   badge/usage.rst
   badge/programming.rst
   badge/application-programming.rst
   badge/firmware.rst
   badge/firmware-development.rst
   badge/badge_link.rst
   badge/bl00mbox.rst

.. toctree::
   :maxdepth: 1
   :caption: API:

   api/audio.rst
   api/badge_link.rst
   api/captouch.rst
   api/hardware.rst
   api/kernel.rst
   api/leds.rst
   api/ctx.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
